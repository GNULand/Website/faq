# Linux FAQ [![pipeline status](https://gitlab.com/Ehsaan/faq/badges/master/pipeline.svg)](https://gitlab.com/Ehsaan/faq/commits/master)
This is a callobrative website for gathering and answering frequently asked questions among newbie users who speak Persian.

## License
The content are licensed under CC-BY-SA-4.0 and FAQ theme (which is design specifically for this project) is licensed under MIT.

## Contributing
1. Fork the project.
2. Clone it to your system.
3. Install NodeJS & npm.
4. Run `npm i` in the project directory.
5. Make your changes.
6. Commit.
7. Send a merge request.

